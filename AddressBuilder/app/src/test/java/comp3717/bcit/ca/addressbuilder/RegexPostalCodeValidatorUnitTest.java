package comp3717.bcit.ca.addressbuilder;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class RegexPostalCodeValidatorUnitTest
{
    @Test
    public void isValidCharacter()
    {
        final RegexPostalCodeValidator validator;

        validator = new RegexPostalCodeValidator(".");
        assertThat(validator.validate("A"), is(true));
        assertThat(validator.validate("9"), is(true));
        assertThat(validator.validate("."), is(true));
        assertThat(validator.validate("("), is(true));
        assertThat(validator.validate("AB"), is(false));
        assertThat(validator.validate("--"), is(false));
        assertThat(validator.validate(""), is(false));
    }

    @Test
    public void isValidDigit()
    {
        final RegexPostalCodeValidator validator;

        validator = new RegexPostalCodeValidator("\\d");
        assertThat(validator.validate("0"), is(true));
        assertThat(validator.validate("1"), is(true));
        assertThat(validator.validate("2"), is(true));
        assertThat(validator.validate("3"), is(true));
        assertThat(validator.validate("4"), is(true));
        assertThat(validator.validate("5"), is(true));
        assertThat(validator.validate("6"), is(true));
        assertThat(validator.validate("7"), is(true));
        assertThat(validator.validate("8"), is(true));
        assertThat(validator.validate("9"), is(true));
        assertThat(validator.validate("10"), is(false));
        assertThat(validator.validate("11"), is(false));
        assertThat(validator.validate("AB"), is(false));
        assertThat(validator.validate("--"), is(false));
        assertThat(validator.validate(""), is(false));
    }

    @Test
    public void isValidNonDigit()
    {
        final RegexPostalCodeValidator validator;

        validator = new RegexPostalCodeValidator("\\D");
        assertThat(validator.validate("A"), is(true));
        assertThat(validator.validate("z"), is(true));
        assertThat(validator.validate("("), is(true));
        assertThat(validator.validate("+"), is(true));
        assertThat(validator.validate("0"), is(false));
        assertThat(validator.validate("1"), is(false));
        assertThat(validator.validate("2"), is(false));
        assertThat(validator.validate("3"), is(false));
        assertThat(validator.validate("4"), is(false));
        assertThat(validator.validate("5"), is(false));
        assertThat(validator.validate("6"), is(false));
        assertThat(validator.validate("7"), is(false));
        assertThat(validator.validate("8"), is(false));
        assertThat(validator.validate("9"), is(false));
        assertThat(validator.validate("10"), is(false));
        assertThat(validator.validate("11"), is(false));
        assertThat(validator.validate("AB"), is(false));
        assertThat(validator.validate("--"), is(false));
        assertThat(validator.validate(""), is(false));
    }

    @Test
    public void isValidCanadianPostalCode()
    {
        final RegexPostalCodeValidator validator;

        validator = new RegexPostalCodeValidator("(\\D\\d){3}?");
        assertThat(validator.validate("A1B2C3"),   is(true));
        assertThat(validator.validate("B2C3D4"),   is(true));
        assertThat(validator.validate("A1B2C"),    is(false));
        assertThat(validator.validate("A1B2C"),    is(false));
        assertThat(validator.validate("A1B2"),     is(false));
        assertThat(validator.validate("A1B"),      is(false));
        assertThat(validator.validate("A1"),       is(false));
        assertThat(validator.validate("A"),        is(false));
        assertThat(validator.validate("123456"),   is(false));
        assertThat(validator.validate("ABCDEF"),   is(false));
        assertThat(validator.validate("A1B2C3D"),  is(false));
        assertThat(validator.validate("A1B2C3D4"), is(false));
    }

    @Test
    public void isValidUnitedStatesPostalCode()
    {
        final RegexPostalCodeValidator validator;

        validator = new RegexPostalCodeValidator("(\\d){5}?");
        assertThat(validator.validate("12345"),   is(true));
        assertThat(validator.validate("23456"),   is(true));
        assertThat(validator.validate("00000"),   is(true));
        assertThat(validator.validate("99999"),   is(true));
        assertThat(validator.validate("9999"),    is(false));
        assertThat(validator.validate("999"),     is(false));
        assertThat(validator.validate("99"),      is(false));
        assertThat(validator.validate("9"),       is(false));
        assertThat(validator.validate(""),        is(false));
        assertThat(validator.validate("999999"),  is(false));
        assertThat(validator.validate("9999999"), is(false));
        assertThat(validator.validate("ABCDE"),   is(false));
    }
}
