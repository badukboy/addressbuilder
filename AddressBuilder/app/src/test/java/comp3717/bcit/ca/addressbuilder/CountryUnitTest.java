package comp3717.bcit.ca.addressbuilder;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CountryUnitTest
{
    @Test
    public void countryByName()
    {
        check("Afghanistan",
              "AF",
              "AFG",
              4);
        check("Aland Islands",
              "AX",
              "ALA",
              248);
        check("Albania",
              "AL",
              "ALB",
              8);
        check("Algeria",
              "DZ",
              "DZA",
              12);
        check("American Samoa",
              "AS",
              "ASM",
              16);
        check("Andorra",
              "AD",
              "AND",
              20);
        check("Angola",
              "AO",
              "AGO",
              24);
        check("Anguilla",
              "AI",
              "AIA",
              660);
        check("Antarctica",
              "AQ",
              "ATA",
              10);
        check("Antigua and Barbuda",
              "AG",
              "ATG",
              28);
        check("Argentina",
              "AR",
              "ARG",
              32);
        check("Armenia",
              "AM",
              "ARM",
              51);
        check("Aruba",
              "AW",
              "ABW",
              533);
        check("Australia",
              "AU",
              "AUS",
              36);
        check("Austria",
              "AT",
              "AUT",
              40);
        check("Azerbaijan",
              "AZ",
              "AZE",
              31);
        check("Bahamas",
              "BS",
              "BHS",
              44);
        check("Bahrain",
              "BH",
              "BHR",
              48);
        check("Bangladesh",
              "BD",
              "BGD",
              50);
        check("Barbados",
              "BB",
              "BRB",
              52);
        check("Belarus",
              "BY",
              "BLR",
              112);
        check("Belgium",
              "BE",
              "BEL",
              56);
        check("Belize",
              "BZ",
              "BLZ",
              84);
        check("Benin",
              "BJ",
              "BEN",
              204);
        check("Bermuda",
              "BM",
              "BMU",
              60);
        check("Bhutan",
              "BT",
              "BTN",
              64);
        check("Bolivia",
              "BO",
              "BOL",
              68);
        check("Bosnia and Herzegovina",
              "BA",
              "BIH",
              70);
        check("Botswana",
              "BW",
              "BWA",
              72);
        check("Bouvet Island",
              "BV",
              "BVT",
              74);
        check("Brazil",
              "BR",
              "BRA",
              76);
        check("British Virgin Islands",
              "VG",
              "VGB",
              92);
        check("British Indian Ocean Territory",
              "IO",
              "IOT",
              86);
        check("Brunei Darussalam",
              "BN",
              "BRN",
              96);
        check("Bulgaria",
              "BG",
              "BGR",
              100);
        check("Burkina Faso",
              "BF",
              "BFA",
              854);
        check("Burundi",
              "BI",
              "BDI",
              108);
        check("Cambodia",
              "KH",
              "KHM",
              116);
        check("Cameroon",
              "CM",
              "CMR",
              120);
        check("Canada",
              "CA",
              "CAN",
              124);
        check("Cape Verde",
              "CV",
              "CPV",
              132);
        check("Cayman Islands",
              "KY",
              "CYM",
              136);
        check("Central African Republic",
              "CF",
              "CAF",
              140);
        check("Chad",
              "TD",
              "TCD",
              148);
        check("Chile",
              "CL",
              "CHL",
              152);
        check("China",
              "CN",
              "CHN",
              156);
        check("Hong Kong, Special Administrative Region of China",
              "HK",
              "HKG",
              344);
        check("Macao, Special Administrative Region of China",
              "MO",
              "MAC",
              446);
        check("Christmas Island",
              "CX",
              "CXR",
              162);
        check("Cocos (Keeling) Islands",
              "CC",
              "CCK",
              166);
        check("Colombia",
              "CO",
              "COL",
              170);
        check("Comoros",
              "KM",
              "COM",
              174);
        check("Congo (Brazzaville)",
              "CG",
              "COG",
              178);
        check("Congo, Democratic Republic of the",
              "CD",
              "COD",
              180);
        check("Cook Islands",
              "CK",
              "COK",
              184);
        check("Costa Rica",
              "CR",
              "CRI",
              188);
        check("Côte d'Ivoire",
              "CI",
              "CIV",
              384);
        check("Croatia",
              "HR",
              "HRV",
              191);
        check("Cuba",
              "CU",
              "CUB",
              192);
        check("Cyprus",
              "CY",
              "CYP",
              196);
        check("Czech Republic",
              "CZ",
              "CZE",
              203);
        check("Denmark",
              "DK",
              "DNK",
              208);
        check("Djibouti",
              "DJ",
              "DJI",
              262);
        check("Dominica",
              "DM",
              "DMA",
              212);
        check("Dominican Republic",
              "DO",
              "DOM",
              214);
        check("Ecuador",
              "EC",
              "ECU",
              218);
        check("Egypt",
              "EG",
              "EGY",
              818);
        check("El Salvador",
              "SV",
              "SLV",
              222);
        check("Equatorial Guinea",
              "GQ",
              "GNQ",
              226);
        check("Eritrea",
              "ER",
              "ERI",
              232);
        check("Estonia",
              "EE",
              "EST",
              233);
        check("Ethiopia",
              "ET",
              "ETH",
              231);
        check("Falkland Islands (Malvinas)",
              "FK",
              "FLK",
              238);
        check("Faroe Islands",
              "FO",
              "FRO",
              234);
        check("Fiji",
              "FJ",
              "FJI",
              242);
        check("Finland",
              "FI",
              "FIN",
              246);
        check("France",
              "FR",
              "FRA",
              250);
        check("French Guiana",
              "GF",
              "GUF",
              254);
        check("French Polynesia",
              "PF",
              "PYF",
              258);
        check("French Southern Territories",
              "TF",
              "ATF",
              260);
        check("Gabon",
              "GA",
              "GAB",
              266);
        check("Gambia",
              "GM",
              "GMB",
              270);
        check("Georgia",
              "GE",
              "GEO",
              268);
        check("Germany",
              "DE",
              "DEU",
              276);
        check("Ghana",
              "GH",
              "GHA",
              288);
        check("Gibraltar",
              "GI",
              "GIB",
              292);
        check("Greece",
              "GR",
              "GRC",
              300);
        check("Greenland",
              "GL",
              "GRL",
              304);
        check("Grenada",
              "GD",
              "GRD",
              308);
        check("Guadeloupe",
              "GP",
              "GLP",
              312);
        check("Guam",
              "GU",
              "GUM",
              316);
        check("Guatemala",
              "GT",
              "GTM",
              320);
        check("Guernsey",
              "GG",
              "GGY",
              831);
        check("Guinea",
              "GN",
              "GIN",
              324);
        check("Guinea-Bissau",
              "GW",
              "GNB",
              624);
        check("Guyana",
              "GY",
              "GUY",
              328);
        check("Haiti",
              "HT",
              "HTI",
              332);
        check("Heard Island and Mcdonald Islands",
              "HM",
              "HMD",
              334);
        check("Holy See (Vatican City State)",
              "VA",
              "VAT",
              336);
        check("Honduras",
              "HN",
              "HND",
              340);
        check("Hungary",
              "HU",
              "HUN",
              348);
        check("Iceland",
              "IS",
              "ISL",
              352);
        check("India",
              "IN",
              "IND",
              356);
        check("Indonesia",
              "ID",
              "IDN",
              360);
        check("Iran, Islamic Republic of",
              "IR",
              "IRN",
              364);
        check("Iraq",
              "IQ",
              "IRQ",
              368);
        check("Ireland",
              "IE",
              "IRL",
              372);
        check("Isle of Man",
              "IM",
              "IMN",
              833);
        check("Israel",
              "IL",
              "ISR",
              376);
        check("Italy",
              "IT",
              "ITA",
              380);
        check("Jamaica",
              "JM",
              "JAM",
              388);
        check("Japan",
              "JP",
              "JPN",
              392);
        check("Jersey",
              "JE",
              "JEY",
              832);
        check("Jordan",
              "JO",
              "JOR",
              400);
        check("Kazakhstan",
              "KZ",
              "KAZ",
              398);
        check("Kenya",
              "KE",
              "KEN",
              404);
        check("Kiribati",
              "KI",
              "KIR",
              296);
        check("Korea, Democratic People's Republic of",
              "KP",
              "PRK",
              408);
        check("Korea, Republic of",
              "KR",
              "KOR",
              410);
        check("Kuwait",
              "KW",
              "KWT",
              414);
        check("Kyrgyzstan",
              "KG",
              "KGZ",
              417);
        check("Lao PDR",
              "LA",
              "LAO",
              418);
        check("Latvia",
              "LV",
              "LVA",
              428);
        check("Lebanon",
              "LB",
              "LBN",
              422);
        check("Lesotho",
              "LS",
              "LSO",
              426);
        check("Liberia",
              "LR",
              "LBR",
              430);
        check("Libya",
              "LY",
              "LBY",
              434);
        check("Liechtenstein",
              "LI",
              "LIE",
              438);
        check("Lithuania",
              "LT",
              "LTU",
              440);
        check("Luxembourg",
              "LU",
              "LUX",
              442);
        check("Macedonia, Republic of",
              "MK",
              "MKD",
              807);
        check("Madagascar",
              "MG",
              "MDG",
              450);
        check("Malawi",
              "MW",
              "MWI",
              454);
        check("Malaysia",
              "MY",
              "MYS",
              458);
        check("Maldives",
              "MV",
              "MDV",
              462);
        check("Mali",
              "ML",
              "MLI",
              466);
        check("Malta",
              "MT",
              "MLT",
              470);
        check("Marshall Islands",
              "MH",
              "MHL",
              584);
        check("Martinique",
              "MQ",
              "MTQ",
              474);
        check("Mauritania",
              "MR",
              "MRT",
              478);
        check("Mauritius",
              "MU",
              "MUS",
              480);
        check("Mayotte",
              "YT",
              "MYT",
              175);
        check("Mexico",
              "MX",
              "MEX",
              484);
        check("Micronesia, Federated States of",
              "FM",
              "FSM",
              583);
        check("Moldova",
              "MD",
              "MDA",
              498);
        check("Monaco",
              "MC",
              "MCO",
              492);
        check("Mongolia",
              "MN",
              "MNG",
              496);
        check("Montenegro",
              "ME",
              "MNE",
              499);
        check("Montserrat",
              "MS",
              "MSR",
              500);
        check("Morocco",
              "MA",
              "MAR",
              504);
        check("Mozambique",
              "MZ",
              "MOZ",
              508);
        check("Myanmar",
              "MM",
              "MMR",
              104);
        check("Namibia",
              "NA",
              "NAM",
              516);
        check("Nauru",
              "NR",
              "NRU",
              520);
        check("Nepal",
              "NP",
              "NPL",
              524);
        check("Netherlands",
              "NL",
              "NLD",
              528);
        check("Netherlands Antilles",
              "AN",
              "ANT",
              530);
        check("New Caledonia",
              "NC",
              "NCL",
              540);
        check("New Zealand",
              "NZ",
              "NZL",
              554);
        check("Nicaragua",
              "NI",
              "NIC",
              558);
        check("Niger",
              "NE",
              "NER",
              562);
        check("Nigeria",
              "NG",
              "NGA",
              566);
        check("Niue",
              "NU",
              "NIU",
              570);
        check("Norfolk Island",
              "NF",
              "NFK",
              574);
        check("Northern Mariana Islands",
              "MP",
              "MNP",
              580);
        check("Norway",
              "NO",
              "NOR",
              578);
        check("Oman",
              "OM",
              "OMN",
              512);
        check("Pakistan",
              "PK",
              "PAK",
              586);
        check("Palau",
              "PW",
              "PLW",
              585);
        check("Palestinian Territory, Occupied",
              "PS",
              "PSE",
              275);
        check("Panama",
              "PA",
              "PAN",
              591);
        check("Papua New Guinea",
              "PG",
              "PNG",
              598);
        check("Paraguay",
              "PY",
              "PRY",
              600);
        check("Peru",
              "PE",
              "PER",
              604);
        check("Philippines",
              "PH",
              "PHL",
              608);
        check("Pitcairn",
              "PN",
              "PCN",
              612);
        check("Poland",
              "PL",
              "POL",
              616);
        check("Portugal",
              "PT",
              "PRT",
              620);
        check("Puerto Rico",
              "PR",
              "PRI",
              630);
        check("Qatar",
              "QA",
              "QAT",
              634);
        check("Réunion",
              "RE",
              "REU",
              638);
        check("Romania",
              "RO",
              "ROU",
              642);
        check("Russian Federation",
              "RU",
              "RUS",
              643);
        check("Rwanda",
              "RW",
              "RWA",
              646);
        check("Saint-Barthélemy",
              "BL",
              "BLM",
              652);
        check("Saint Helena",
              "SH",
              "SHN",
              654);
        check("Saint Kitts and Nevis",
              "KN",
              "KNA",
              659);
        check("Saint Lucia",
              "LC",
              "LCA",
              662);
        check("Saint-Martin (French part)",
              "MF",
              "MAF",
              663);
        check("Saint Pierre and Miquelon",
              "PM",
              "SPM",
              666);
        check("Saint Vincent and Grenadines",
              "VC",
              "VCT",
              670);
        check("Samoa",
              "WS",
              "WSM",
              882);
        check("San Marino",
              "SM",
              "SMR",
              674);
        check("Sao Tome and Principe",
              "ST",
              "STP",
              678);
        check("Saudi Arabia",
              "SA",
              "SAU",
              682);
        check("Senegal",
              "SN",
              "SEN",
              686);
        check("Serbia",
              "RS",
              "SRB",
              688);
        check("Seychelles",
              "SC",
              "SYC",
              690);
        check("Sierra Leone",
              "SL",
              "SLE",
              694);
        check("Singapore",
              "SG",
              "SGP",
              702);
        check("Slovakia",
              "SK",
              "SVK",
              703);
        check("Slovenia",
              "SI",
              "SVN",
              705);
        check("Solomon Islands",
              "SB",
              "SLB",
              90);
        check("Somalia",
              "SO",
              "SOM",
              706);
        check("South Africa",
              "ZA",
              "ZAF",
              710);
        check("South Georgia and the South Sandwich Islands",
              "GS",
              "SGS",
              239);
        check("South Sudan",
              "SS",
              "SSD",
              728);
        check("Spain",
              "ES",
              "ESP",
              724);
        check("Sri Lanka",
              "LK",
              "LKA",
              144);
        check("Sudan",
              "SD",
              "SDN",
              736);
        check("Suriname",
              "SR",
              "SUR",
              740);
        check("Svalbard and Jan Mayen Islands",
              "SJ",
              "SJM",
              744);
        check("Swaziland",
              "SZ",
              "SWZ",
              748);
        check("Sweden",
              "SE",
              "SWE",
              752);
        check("Switzerland",
              "CH",
              "CHE",
              756);
        check("Syrian Arab Republic (Syria)",
              "SY",
              "SYR",
              760);
        check("Taiwan, Republic of China",
              "TW",
              "TWN",
              158);
        check("Tajikistan",
              "TJ",
              "TJK",
              762);
        check("Tanzania, United Republic of",
              "TZ",
              "TZA",
              834);
        check("Thailand",
              "TH",
              "THA",
              764);
        check("Timor-Leste",
              "TL",
              "TLS",
              626);
        check("Togo",
              "TG",
              "TGO",
              768);
        check("Tokelau",
              "TK",
              "TKL",
              772);
        check("Tonga",
              "TO",
              "TON",
              776);
        check("Trinidad and Tobago",
              "TT",
              "TTO",
              780);
        check("Tunisia",
              "TN",
              "TUN",
              788);
        check("Turkey",
              "TR",
              "TUR",
              792);
        check("Turkmenistan",
              "TM",
              "TKM",
              795);
        check("Turks and Caicos Islands",
              "TC",
              "TCA",
              796);
        check("Tuvalu",
              "TV",
              "TUV",
              798);
        check("Uganda",
              "UG",
              "UGA",
              800);
        check("Ukraine",
              "UA",
              "UKR",
              804);
        check("United Arab Emirates",
              "AE",
              "ARE",
              784);
        check("United Kingdom",
              "GB",
              "GBR",
              826);
        check("United States of America",
              "US",
              "USA",
              840);
        check("United States Minor Outlying Islands",
              "UM",
              "UMI",
              581);
        check("Uruguay",
              "UY",
              "URY",
              858);
        check("Uzbekistan",
              "UZ",
              "UZB",
              860);
        check("Vanuatu",
              "VU",
              "VUT",
              548);
        check("Venezuela (Bolivarian Republic of)",
              "VE",
              "VEN",
              862);
        check("Viet Nam",
              "VN",
              "VNM",
              704);
        check("Virgin Islands, US",
              "VI",
              "VIR",
              850);
        check("Wallis and Futuna Islands",
              "WF",
              "WLF",
              876);
        check("Western Sahara",
              "EH",
              "ESH",
              732);
        check("Yemen",
              "YE",
              "YEM",
              887);
        check("Zambia",
              "ZM",
              "ZMB",
              894);
        check("Zimbabwe",
              "ZW",
              "ZWE",
              716);
    }

    private void check(final String expectedName,
                       final String expectedISO2,
                       final String expectedISO3,
                       final int    expectedISONumber)
    {
        checkByName(expectedName,
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByName(expectedName.toLowerCase(),
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByName(expectedName.toUpperCase(),
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByISO2(expectedISO2,
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByISO2(expectedISO2.toLowerCase(),
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByISO2(expectedISO2.toUpperCase(),
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByISO3(expectedISO3,
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByISO3(expectedISO3.toLowerCase(),
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByISO3(expectedISO3.toUpperCase(),
                    expectedName,
                    expectedISO2,
                    expectedISO3,
                    expectedISONumber);
        checkByISONumber(expectedISONumber,
                         expectedName,
                         expectedISO2,
                         expectedISO3,
                         expectedISONumber);
    }

    private void checkByName(final String name,
                             final String expectedName,
                             final String expectedISO2,
                             final String expectedISO3,
                             final int    expectedISONumber)
    {
        final Country country;

        country = Country.getCountryByName(name);
        check(country,
              expectedName,
              expectedISO2,
              expectedISO3,
              expectedISONumber);
    }

    private void checkByISO2(final String iso2,
                             final String expectedName,
                             final String expectedISO2,
                             final String expectedISO3,
                             final int    expectedISONumber)
    {
        final Country country;

        country = Country.getCountryByISO2(iso2);
        check(country,
              expectedName,
              expectedISO2,
              expectedISO3,
              expectedISONumber);
    }

    private void checkByISO3(final String iso3,
                             final String expectedName,
                             final String expectedISO2,
                             final String expectedISO3,
                             final int    expectedISONumber)
    {
        final Country country;

        country = Country.getCountryByISO3(iso3);
        check(country,
              expectedName,
              expectedISO2,
              expectedISO3,
              expectedISONumber);
    }

    private void checkByISONumber(final int    isoNumber,
                                  final String expectedName,
                                  final String expectedISO2,
                                  final String expectedISO3,
                                  final int    expectedISONumber)
    {
        final Country country;

        country = Country.getCountryByISONumber(isoNumber);
        check(country,
              expectedName,
              expectedISO2,
              expectedISO3,
              expectedISONumber);
    }

    private void check(final Country country,
                       final String  expectedName,
                       final String  expectedISO2,
                       final String  expectedISO3,
                       final int     expectedISONumber)
    {
        assertThat(country.getName(),       is(expectedName));
        assertThat(country.getISO2(),       is(expectedISO2));
        assertThat(country.getISO3(),       is(expectedISO3));
        assertThat(country.getISONumeric(), is(expectedISONumber));
    }
}
