package comp3717.bcit.ca.addressbuilder;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class NullPostalCodeValidatorUnitTest
{
    @Test
    public void isValid()
    {
        assertThat(NullPostalCodeValidator.INSTANCE.validate(null), is(true));
        assertThat(NullPostalCodeValidator.INSTANCE.validate(""), is(true));
        assertThat(NullPostalCodeValidator.INSTANCE.validate("1"), is(true));
        assertThat(NullPostalCodeValidator.INSTANCE.validate("abc"), is(true));
        assertThat(NullPostalCodeValidator.INSTANCE.validate("adkljad[u8314jkqsaid0]94132e"), is(true));
    }
}
