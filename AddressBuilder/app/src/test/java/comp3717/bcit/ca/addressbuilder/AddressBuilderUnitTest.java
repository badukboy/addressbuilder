package comp3717.bcit.ca.addressbuilder;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class AddressBuilderUnitTest
{
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void validAddress()
        throws InvalidAddressException
    {
        final AddressBuilder builder;
        final String addressA;
        final String addressB;
        final String addressC;

        builder = new AddressBuilder();
        addressA = builder.buildAddress("John",
                                        "Doe",
                                        123,
                                        "Some Street",
                                        "Some City",
                                        "Some Province",
                                        Country.getCountryByISO3("USA"),
                                        "12345");

        assertThat(addressA, is("Doe, John" + File.separator +
                                "123, Some Street" + File.separator +
                                "Some City" + File.separator +
                                "Some Province" + File.separator +
                                "United States of America" + File.separator +
                                "12345"));

        addressB = builder.buildAddress("Jane",
                                        "Doe",
                                        43,
                                        "A Street",
                                        "A City",
                                        "A Province",
                                        Country.getCountryByName("Canada"),
                                        "V1V1V1");

        assertThat(addressB, is("Doe, Jane" + File.separator +
                                "43, A Street" + File.separator +
                                "A City" + File.separator +
                                "A Province" + File.separator +
                                "Canada" + File.separator +
                                "V1V1V1"));

        addressC = builder.buildAddress("A",
                                        "B",
                                        1,
                                        "C",
                                        "D",
                                        "E",
                                        Country.getCountryByISO2("GB"),
                                        "G");

        assertThat(addressC, is("B, A" + File.separator +
                                "1, C" + File.separator +
                                "D" + File.separator +
                                "E" + File.separator +
                                "United Kingdom" + File.separator +
                                "G"));
    }

    @Test
    public void nullFirstName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("firstName cannot be null");
        builder.buildAddress(null,
                             "B",
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void emptyFirstName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("firstName cannot be empty");
        builder.buildAddress("",
                             "B",
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void nullLastName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("lastName cannot be null");
        builder.buildAddress("A",
                             null,
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void emptyLastName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("lastName cannot be empty");
        builder.buildAddress("A",
                             "",
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void invalidBuildingNumber0()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("buildingNumber must be > 0, was: 0");
        builder.buildAddress("A",
                             "B",
                             0,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void invalidBuildingNumber1()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("buildingNumber must be > 0, was: -1");
        builder.buildAddress("A",
                             "B",
                             -1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void nullStreetName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("streetName cannot be null");
        builder.buildAddress("A",
                             "B",
                             1,
                             null,
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void emptyStreetName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("streetName cannot be empty");
        builder.buildAddress("A",
                             "B",
                             1,
                             "",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void nullCityName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("cityName cannot be null");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             null,
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void emptyCityName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("cityName cannot be empty");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void nullProvinceName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("provinceName cannot be null");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "D",
                             null,
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void emptyProvinceName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("provinceName cannot be empty");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "D",
                             "",
                             Country.getCountryByISO3("USA"),
                             "G");
    }

    @Test
    public void nullCountryName()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("country cannot be null");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "D",
                             "E",
                             null,
                             "G");
    }

    @Test
    public void nullPostalCode()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("postalCode cannot be null");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             null);
    }

    @Test
    public void emptyPostalCode()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("postalCode cannot be empty");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO3("USA"),
                             "");
    }

    @Test
    public void badCanadianPostalCode()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("postalCode is not valid for: Canada");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByName("Canada"),
                             "V1V11V");
    }

    @Test
    public void badUSAPostalCode()
        throws InvalidAddressException
    {
        final AddressBuilder builder;

        builder = new AddressBuilder();

        thrown.expect(InvalidAddressException.class);
        thrown.expectMessage("postalCode is not valid for: United States of America");
        builder.buildAddress("A",
                             "B",
                             1,
                             "C",
                             "D",
                             "E",
                             Country.getCountryByISO2("US"),
                             "123456");
    }
}
