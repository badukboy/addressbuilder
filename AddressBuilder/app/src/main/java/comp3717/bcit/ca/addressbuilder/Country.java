package comp3717.bcit.ca.addressbuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by darcy on 2016-10-14.
 */

public final class Country
{
    private static final Map<String, Country>  countriesByName;
    private static final Map<String, Country>  countriesByISO2;
    private static final Map<String, Country>  countriesByISO3;
    private static final Map<Integer, Country> countriesByISONumber;

    private final PostalCodeValidator postalCodeValidator;
    private final String              name;
    private final String              iso2;
    private final String              iso3;
    private final int                 isoNumeric;

    static
    {
        countriesByName = new HashMap<>();
        countriesByISO2 = new HashMap<>();
        countriesByISO3 = new HashMap<>();
        countriesByISONumber = new HashMap<>();

        // http://www.nationsonline.org/oneworld/country_code_list.htm
        add("Afghanistan",
            "AF",
            "AFG",
            4,
            NullPostalCodeValidator.INSTANCE);
        add("Aland Islands",
            "AX",
            "ALA",
            248,
            NullPostalCodeValidator.INSTANCE);
        add("Albania",
            "AL",
            "ALB",
            8,
            NullPostalCodeValidator.INSTANCE);
        add("Algeria",
            "DZ",
            "DZA",
            12,
            NullPostalCodeValidator.INSTANCE);
        add("American Samoa",
            "AS",
            "ASM",
            16,
            NullPostalCodeValidator.INSTANCE);
        add("Andorra",
            "AD",
            "AND",
            20,
            NullPostalCodeValidator.INSTANCE);
        add("Angola",
            "AO",
            "AGO",
            24,
            NullPostalCodeValidator.INSTANCE);
        add("Anguilla",
            "AI",
            "AIA",
            660,
            NullPostalCodeValidator.INSTANCE);
        add("Antarctica",
            "AQ",
            "ATA",
            10,
            NullPostalCodeValidator.INSTANCE);
        add("Antigua and Barbuda",
            "AG",
            "ATG",
            28,
            NullPostalCodeValidator.INSTANCE);
        add("Argentina",
            "AR",
            "ARG",
            32,
            NullPostalCodeValidator.INSTANCE);
        add("Armenia",
            "AM",
            "ARM",
            51,
            NullPostalCodeValidator.INSTANCE);
        add("Aruba",
            "AW",
            "ABW",
            533,
            NullPostalCodeValidator.INSTANCE);
        add("Australia",
            "AU",
            "AUS",
            36,
            NullPostalCodeValidator.INSTANCE);
        add("Austria",
            "AT",
            "AUT",
            40,
            NullPostalCodeValidator.INSTANCE);
        add("Azerbaijan",
            "AZ",
            "AZE",
            31,
            NullPostalCodeValidator.INSTANCE);
        add("Bahamas",
            "BS",
            "BHS",
            44,
            NullPostalCodeValidator.INSTANCE);
        add("Bahrain",
            "BH",
            "BHR",
            48,
            NullPostalCodeValidator.INSTANCE);
        add("Bangladesh",
            "BD",
            "BGD",
            50,
            NullPostalCodeValidator.INSTANCE);
        add("Barbados",
            "BB",
            "BRB",
            52,
            NullPostalCodeValidator.INSTANCE);
        add("Belarus",
            "BY",
            "BLR",
            112,
            NullPostalCodeValidator.INSTANCE);
        add("Belgium",
            "BE",
            "BEL",
            56,
            NullPostalCodeValidator.INSTANCE);
        add("Belize",
            "BZ",
            "BLZ",
            84,
            NullPostalCodeValidator.INSTANCE);
        add("Benin",
            "BJ",
            "BEN",
            204,
            NullPostalCodeValidator.INSTANCE);
        add("Bermuda",
            "BM",
            "BMU",
            60,
            NullPostalCodeValidator.INSTANCE);
        add("Bhutan",
            "BT",
            "BTN",
            64,
            NullPostalCodeValidator.INSTANCE);
        add("Bolivia",
            "BO",
            "BOL",
            68,
            NullPostalCodeValidator.INSTANCE);
        add("Bosnia and Herzegovina",
            "BA",
            "BIH",
            70,
            NullPostalCodeValidator.INSTANCE);
        add("Botswana",
            "BW",
            "BWA",
            72,
            NullPostalCodeValidator.INSTANCE);
        add("Bouvet Island",
            "BV",
            "BVT",
            74,
            NullPostalCodeValidator.INSTANCE);
        add("Brazil",
            "BR",
            "BRA",
            76,
            NullPostalCodeValidator.INSTANCE);
        add("British Virgin Islands",
            "VG",
            "VGB",
            92,
            NullPostalCodeValidator.INSTANCE);
        add("British Indian Ocean Territory",
            "IO",
            "IOT",
            86,
            NullPostalCodeValidator.INSTANCE);
        add("Brunei Darussalam",
            "BN",
            "BRN",
            96,
            NullPostalCodeValidator.INSTANCE);
        add("Bulgaria",
            "BG",
            "BGR",
            100,
            NullPostalCodeValidator.INSTANCE);
        add("Burkina Faso",
            "BF",
            "BFA",
            854,
            NullPostalCodeValidator.INSTANCE);
        add("Burundi",
            "BI",
            "BDI",
            108,
            NullPostalCodeValidator.INSTANCE);
        add("Cambodia",
            "KH",
            "KHM",
            116,
            NullPostalCodeValidator.INSTANCE);
        add("Cameroon",
            "CM",
            "CMR",
            120,
            NullPostalCodeValidator.INSTANCE);
        add("Canada",
            "CA",
            "CAN",
            124,
            new RegexPostalCodeValidator("(\\D\\d){3}?"));
        add("Cape Verde",
            "CV",
            "CPV",
            132,
            NullPostalCodeValidator.INSTANCE);
        add("Cayman Islands",
            "KY",
            "CYM",
            136,
            NullPostalCodeValidator.INSTANCE);
        add("Central African Republic",
            "CF",
            "CAF",
            140,
            NullPostalCodeValidator.INSTANCE);
        add("Chad",
            "TD",
            "TCD",
            148,
            NullPostalCodeValidator.INSTANCE);
        add("Chile",
            "CL",
            "CHL",
            152,
            NullPostalCodeValidator.INSTANCE);
        add("China",
            "CN",
            "CHN",
            156,
            NullPostalCodeValidator.INSTANCE);
        add("Hong Kong, Special Administrative Region of China",
            "HK",
            "HKG",
            344,
            NullPostalCodeValidator.INSTANCE);
        add("Macao, Special Administrative Region of China",
            "MO",
            "MAC",
            446,
            NullPostalCodeValidator.INSTANCE);
        add("Christmas Island",
            "CX",
            "CXR",
            162,
            NullPostalCodeValidator.INSTANCE);
        add("Cocos (Keeling) Islands",
            "CC",
            "CCK",
            166,
            NullPostalCodeValidator.INSTANCE);
        add("Colombia",
            "CO",
            "COL",
            170,
            NullPostalCodeValidator.INSTANCE);
        add("Comoros",
            "KM",
            "COM",
            174,
            NullPostalCodeValidator.INSTANCE);
        add("Congo (Brazzaville)",
            "CG",
            "COG",
            178,
            NullPostalCodeValidator.INSTANCE);
        add("Congo, Democratic Republic of the",
            "CD",
            "COD",
            180,
            NullPostalCodeValidator.INSTANCE);
        add("Cook Islands",
            "CK",
            "COK",
            184,
            NullPostalCodeValidator.INSTANCE);
        add("Costa Rica",
            "CR",
            "CRI",
            188,
            NullPostalCodeValidator.INSTANCE);
        add("Côte d'Ivoire",
            "CI",
            "CIV",
            384,
            NullPostalCodeValidator.INSTANCE);
        add("Croatia",
            "HR",
            "HRV",
            191,
            NullPostalCodeValidator.INSTANCE);
        add("Cuba",
            "CU",
            "CUB",
            192,
            NullPostalCodeValidator.INSTANCE);
        add("Cyprus",
            "CY",
            "CYP",
            196,
            NullPostalCodeValidator.INSTANCE);
        add("Czech Republic",
            "CZ",
            "CZE",
            203,
            NullPostalCodeValidator.INSTANCE);
        add("Denmark",
            "DK",
            "DNK",
            208,
            NullPostalCodeValidator.INSTANCE);
        add("Djibouti",
            "DJ",
            "DJI",
            262,
            NullPostalCodeValidator.INSTANCE);
        add("Dominica",
            "DM",
            "DMA",
            212,
            NullPostalCodeValidator.INSTANCE);
        add("Dominican Republic",
            "DO",
            "DOM",
            214,
            NullPostalCodeValidator.INSTANCE);
        add("Ecuador",
            "EC",
            "ECU",
            218,
            NullPostalCodeValidator.INSTANCE);
        add("Egypt",
            "EG",
            "EGY",
            818,
            NullPostalCodeValidator.INSTANCE);
        add("El Salvador",
            "SV",
            "SLV",
            222,
            NullPostalCodeValidator.INSTANCE);
        add("Equatorial Guinea",
            "GQ",
            "GNQ",
            226,
            NullPostalCodeValidator.INSTANCE);
        add("Eritrea",
            "ER",
            "ERI",
            232,
            NullPostalCodeValidator.INSTANCE);
        add("Estonia",
            "EE",
            "EST",
            233,
            NullPostalCodeValidator.INSTANCE);
        add("Ethiopia",
            "ET",
            "ETH",
            231,
            NullPostalCodeValidator.INSTANCE);
        add("Falkland Islands (Malvinas)",
            "FK",
            "FLK",
            238,
            NullPostalCodeValidator.INSTANCE);
        add("Faroe Islands",
            "FO",
            "FRO",
            234,
            NullPostalCodeValidator.INSTANCE);
        add("Fiji",
            "FJ",
            "FJI",
            242,
            NullPostalCodeValidator.INSTANCE);
        add("Finland",
            "FI",
            "FIN",
            246,
            NullPostalCodeValidator.INSTANCE);
        add("France",
            "FR",
            "FRA",
            250,
            NullPostalCodeValidator.INSTANCE);
        add("French Guiana",
            "GF",
            "GUF",
            254,
            NullPostalCodeValidator.INSTANCE);
        add("French Polynesia",
            "PF",
            "PYF",
            258,
            NullPostalCodeValidator.INSTANCE);
        add("French Southern Territories",
            "TF",
            "ATF",
            260,
            NullPostalCodeValidator.INSTANCE);
        add("Gabon",
            "GA",
            "GAB",
            266,
            NullPostalCodeValidator.INSTANCE);
        add("Gambia",
            "GM",
            "GMB",
            270,
            NullPostalCodeValidator.INSTANCE);
        add("Georgia",
            "GE",
            "GEO",
            268,
            NullPostalCodeValidator.INSTANCE);
        add("Germany",
            "DE",
            "DEU",
            276,
            NullPostalCodeValidator.INSTANCE);
        add("Ghana",
            "GH",
            "GHA",
            288,
            NullPostalCodeValidator.INSTANCE);
        add("Gibraltar",
            "GI",
            "GIB",
            292,
            NullPostalCodeValidator.INSTANCE);
        add("Greece",
            "GR",
            "GRC",
            300,
            NullPostalCodeValidator.INSTANCE);
        add("Greenland",
            "GL",
            "GRL",
            304,
            NullPostalCodeValidator.INSTANCE);
        add("Grenada",
            "GD",
            "GRD",
            308,
            NullPostalCodeValidator.INSTANCE);
        add("Guadeloupe",
            "GP",
            "GLP",
            312,
            NullPostalCodeValidator.INSTANCE);
        add("Guam",
            "GU",
            "GUM",
            316,
            NullPostalCodeValidator.INSTANCE);
        add("Guatemala",
            "GT",
            "GTM",
            320,
            NullPostalCodeValidator.INSTANCE);
        add("Guernsey",
            "GG",
            "GGY",
            831,
            NullPostalCodeValidator.INSTANCE);
        add("Guinea",
            "GN",
            "GIN",
            324,
            NullPostalCodeValidator.INSTANCE);
        add("Guinea-Bissau",
            "GW",
            "GNB",
            624,
            NullPostalCodeValidator.INSTANCE);
        add("Guyana",
            "GY",
            "GUY",
            328,
            NullPostalCodeValidator.INSTANCE);
        add("Haiti",
            "HT",
            "HTI",
            332,
            NullPostalCodeValidator.INSTANCE);
        add("Heard Island and Mcdonald Islands",
            "HM",
            "HMD",
            334,
            NullPostalCodeValidator.INSTANCE);
        add("Holy See (Vatican City State)",
            "VA",
            "VAT",
            336,
            NullPostalCodeValidator.INSTANCE);
        add("Honduras",
            "HN",
            "HND",
            340,
            NullPostalCodeValidator.INSTANCE);
        add("Hungary",
            "HU",
            "HUN",
            348,
            NullPostalCodeValidator.INSTANCE);
        add("Iceland",
            "IS",
            "ISL",
            352,
            NullPostalCodeValidator.INSTANCE);
        add("India",
            "IN",
            "IND",
            356,
            NullPostalCodeValidator.INSTANCE);
        add("Indonesia",
            "ID",
            "IDN",
            360,
            NullPostalCodeValidator.INSTANCE);
        add("Iran, Islamic Republic of",
            "IR",
            "IRN",
            364,
            NullPostalCodeValidator.INSTANCE);
        add("Iraq",
            "IQ",
            "IRQ",
            368,
            NullPostalCodeValidator.INSTANCE);
        add("Ireland",
            "IE",
            "IRL",
            372,
            NullPostalCodeValidator.INSTANCE);
        add("Isle of Man",
            "IM",
            "IMN",
            833,
            NullPostalCodeValidator.INSTANCE);
        add("Israel",
            "IL",
            "ISR",
            376,
            NullPostalCodeValidator.INSTANCE);
        add("Italy",
            "IT",
            "ITA",
            380,
            NullPostalCodeValidator.INSTANCE);
        add("Jamaica",
            "JM",
            "JAM",
            388,
            NullPostalCodeValidator.INSTANCE);
        add("Japan",
            "JP",
            "JPN",
            392,
            NullPostalCodeValidator.INSTANCE);
        add("Jersey",
            "JE",
            "JEY",
            832,
            NullPostalCodeValidator.INSTANCE);
        add("Jordan",
            "JO",
            "JOR",
            400,
            NullPostalCodeValidator.INSTANCE);
        add("Kazakhstan",
            "KZ",
            "KAZ",
            398,
            NullPostalCodeValidator.INSTANCE);
        add("Kenya",
            "KE",
            "KEN",
            404,
            NullPostalCodeValidator.INSTANCE);
        add("Kiribati",
            "KI",
            "KIR",
            296,
            NullPostalCodeValidator.INSTANCE);
        add("Korea, Democratic People's Republic of",
            "KP",
            "PRK",
            408,
            NullPostalCodeValidator.INSTANCE);
        add("Korea, Republic of",
            "KR",
            "KOR",
            410,
            NullPostalCodeValidator.INSTANCE);
        add("Kuwait",
            "KW",
            "KWT",
            414,
            NullPostalCodeValidator.INSTANCE);
        add("Kyrgyzstan",
            "KG",
            "KGZ",
            417,
            NullPostalCodeValidator.INSTANCE);
        add("Lao PDR",
            "LA",
            "LAO",
            418,
            NullPostalCodeValidator.INSTANCE);
        add("Latvia",
            "LV",
            "LVA",
            428,
            NullPostalCodeValidator.INSTANCE);
        add("Lebanon",
            "LB",
            "LBN",
            422,
            NullPostalCodeValidator.INSTANCE);
        add("Lesotho",
            "LS",
            "LSO",
            426,
            NullPostalCodeValidator.INSTANCE);
        add("Liberia",
            "LR",
            "LBR",
            430,
            NullPostalCodeValidator.INSTANCE);
        add("Libya",
            "LY",
            "LBY",
            434,
            NullPostalCodeValidator.INSTANCE);
        add("Liechtenstein",
            "LI",
            "LIE",
            438,
            NullPostalCodeValidator.INSTANCE);
        add("Lithuania",
            "LT",
            "LTU",
            440,
            NullPostalCodeValidator.INSTANCE);
        add("Luxembourg",
            "LU",
            "LUX",
            442,
            NullPostalCodeValidator.INSTANCE);
        add("Macedonia, Republic of",
            "MK",
            "MKD",
            807,
            NullPostalCodeValidator.INSTANCE);
        add("Madagascar",
            "MG",
            "MDG",
            450,
            NullPostalCodeValidator.INSTANCE);
        add("Malawi",
            "MW",
            "MWI",
            454,
            NullPostalCodeValidator.INSTANCE);
        add("Malaysia",
            "MY",
            "MYS",
            458,
            NullPostalCodeValidator.INSTANCE);
        add("Maldives",
            "MV",
            "MDV",
            462,
            NullPostalCodeValidator.INSTANCE);
        add("Mali",
            "ML",
            "MLI",
            466,
            NullPostalCodeValidator.INSTANCE);
        add("Malta",
            "MT",
            "MLT",
            470,
            NullPostalCodeValidator.INSTANCE);
        add("Marshall Islands",
            "MH",
            "MHL",
            584,
            NullPostalCodeValidator.INSTANCE);
        add("Martinique",
            "MQ",
            "MTQ",
            474,
            NullPostalCodeValidator.INSTANCE);
        add("Mauritania",
            "MR",
            "MRT",
            478,
            NullPostalCodeValidator.INSTANCE);
        add("Mauritius",
            "MU",
            "MUS",
            480,
            NullPostalCodeValidator.INSTANCE);
        add("Mayotte",
            "YT",
            "MYT",
            175,
            NullPostalCodeValidator.INSTANCE);
        add("Mexico",
            "MX",
            "MEX",
            484,
            NullPostalCodeValidator.INSTANCE);
        add("Micronesia, Federated States of",
            "FM",
            "FSM",
            583,
            NullPostalCodeValidator.INSTANCE);
        add("Moldova",
            "MD",
            "MDA",
            498,
            NullPostalCodeValidator.INSTANCE);
        add("Monaco",
            "MC",
            "MCO",
            492,
            NullPostalCodeValidator.INSTANCE);
        add("Mongolia",
            "MN",
            "MNG",
            496,
            NullPostalCodeValidator.INSTANCE);
        add("Montenegro",
            "ME",
            "MNE",
            499,
            NullPostalCodeValidator.INSTANCE);
        add("Montserrat",
            "MS",
            "MSR",
            500,
            NullPostalCodeValidator.INSTANCE);
        add("Morocco",
            "MA",
            "MAR",
            504,
            NullPostalCodeValidator.INSTANCE);
        add("Mozambique",
            "MZ",
            "MOZ",
            508,
            NullPostalCodeValidator.INSTANCE);
        add("Myanmar",
            "MM",
            "MMR",
            104,
            NullPostalCodeValidator.INSTANCE);
        add("Namibia",
            "NA",
            "NAM",
            516,
            NullPostalCodeValidator.INSTANCE);
        add("Nauru",
            "NR",
            "NRU",
            520,
            NullPostalCodeValidator.INSTANCE);
        add("Nepal",
            "NP",
            "NPL",
            524,
            NullPostalCodeValidator.INSTANCE);
        add("Netherlands",
            "NL",
            "NLD",
            528,
            NullPostalCodeValidator.INSTANCE);
        add("Netherlands Antilles",
            "AN",
            "ANT",
            530,
            NullPostalCodeValidator.INSTANCE);
        add("New Caledonia",
            "NC",
            "NCL",
            540,
            NullPostalCodeValidator.INSTANCE);
        add("New Zealand",
            "NZ",
            "NZL",
            554,
            NullPostalCodeValidator.INSTANCE);
        add("Nicaragua",
            "NI",
            "NIC",
            558,
            NullPostalCodeValidator.INSTANCE);
        add("Niger",
            "NE",
            "NER",
            562,
            NullPostalCodeValidator.INSTANCE);
        add("Nigeria",
            "NG",
            "NGA",
            566,
            NullPostalCodeValidator.INSTANCE);
        add("Niue",
            "NU",
            "NIU",
            570,
            NullPostalCodeValidator.INSTANCE);
        add("Norfolk Island",
            "NF",
            "NFK",
            574,
            NullPostalCodeValidator.INSTANCE);
        add("Northern Mariana Islands",
            "MP",
            "MNP",
            580,
            NullPostalCodeValidator.INSTANCE);
        add("Norway",
            "NO",
            "NOR",
            578,
            NullPostalCodeValidator.INSTANCE);
        add("Oman",
            "OM",
            "OMN",
            512,
            NullPostalCodeValidator.INSTANCE);
        add("Pakistan",
            "PK",
            "PAK",
            586,
            NullPostalCodeValidator.INSTANCE);
        add("Palau",
            "PW",
            "PLW",
            585,
            NullPostalCodeValidator.INSTANCE);
        add("Palestinian Territory, Occupied",
            "PS",
            "PSE",
            275,
            NullPostalCodeValidator.INSTANCE);
        add("Panama",
            "PA",
            "PAN",
            591,
            NullPostalCodeValidator.INSTANCE);
        add("Papua New Guinea",
            "PG",
            "PNG",
            598,
            NullPostalCodeValidator.INSTANCE);
        add("Paraguay",
            "PY",
            "PRY",
            600,
            NullPostalCodeValidator.INSTANCE);
        add("Peru",
            "PE",
            "PER",
            604,
            NullPostalCodeValidator.INSTANCE);
        add("Philippines",
            "PH",
            "PHL",
            608,
            NullPostalCodeValidator.INSTANCE);
        add("Pitcairn",
            "PN",
            "PCN",
            612,
            NullPostalCodeValidator.INSTANCE);
        add("Poland",
            "PL",
            "POL",
            616,
            NullPostalCodeValidator.INSTANCE);
        add("Portugal",
            "PT",
            "PRT",
            620,
            NullPostalCodeValidator.INSTANCE);
        add("Puerto Rico",
            "PR",
            "PRI",
            630,
            NullPostalCodeValidator.INSTANCE);
        add("Qatar",
            "QA",
            "QAT",
            634,
            NullPostalCodeValidator.INSTANCE);
        add("Réunion",
            "RE",
            "REU",
            638,
            NullPostalCodeValidator.INSTANCE);
        add("Romania",
            "RO",
            "ROU",
            642,
            NullPostalCodeValidator.INSTANCE);
        add("Russian Federation",
            "RU",
            "RUS",
            643,
            NullPostalCodeValidator.INSTANCE);
        add("Rwanda",
            "RW",
            "RWA",
            646,
            NullPostalCodeValidator.INSTANCE);
        add("Saint-Barthélemy",
            "BL",
            "BLM",
            652,
            NullPostalCodeValidator.INSTANCE);
        add("Saint Helena",
            "SH",
            "SHN",
            654,
            NullPostalCodeValidator.INSTANCE);
        add("Saint Kitts and Nevis",
            "KN",
            "KNA",
            659,
            NullPostalCodeValidator.INSTANCE);
        add("Saint Lucia",
            "LC",
            "LCA",
            662,
            NullPostalCodeValidator.INSTANCE);
        add("Saint-Martin (French part)",
            "MF",
            "MAF",
            663,
            NullPostalCodeValidator.INSTANCE);
        add("Saint Pierre and Miquelon",
            "PM",
            "SPM",
            666,
            NullPostalCodeValidator.INSTANCE);
        add("Saint Vincent and Grenadines",
            "VC",
            "VCT",
            670,
            NullPostalCodeValidator.INSTANCE);
        add("Samoa",
            "WS",
            "WSM",
            882,
            NullPostalCodeValidator.INSTANCE);
        add("San Marino",
            "SM",
            "SMR",
            674,
            NullPostalCodeValidator.INSTANCE);
        add("Sao Tome and Principe",
            "ST",
            "STP",
            678,
            NullPostalCodeValidator.INSTANCE);
        add("Saudi Arabia",
            "SA",
            "SAU",
            682,
            NullPostalCodeValidator.INSTANCE);
        add("Senegal",
            "SN",
            "SEN",
            686,
            NullPostalCodeValidator.INSTANCE);
        add("Serbia",
            "RS",
            "SRB",
            688,
            NullPostalCodeValidator.INSTANCE);
        add("Seychelles",
            "SC",
            "SYC",
            690,
            NullPostalCodeValidator.INSTANCE);
        add("Sierra Leone",
            "SL",
            "SLE",
            694,
            NullPostalCodeValidator.INSTANCE);
        add("Singapore",
            "SG",
            "SGP",
            702,
            NullPostalCodeValidator.INSTANCE);
        add("Slovakia",
            "SK",
            "SVK",
            703,
            NullPostalCodeValidator.INSTANCE);
        add("Slovenia",
            "SI",
            "SVN",
            705,
            NullPostalCodeValidator.INSTANCE);
        add("Solomon Islands",
            "SB",
            "SLB",
            90,
            NullPostalCodeValidator.INSTANCE);
        add("Somalia",
            "SO",
            "SOM",
            706,
            NullPostalCodeValidator.INSTANCE);
        add("South Africa",
            "ZA",
            "ZAF",
            710,
            NullPostalCodeValidator.INSTANCE);
        add("South Georgia and the South Sandwich Islands",
            "GS",
            "SGS",
            239,
            NullPostalCodeValidator.INSTANCE);
        add("South Sudan",
            "SS",
            "SSD",
            728,
            NullPostalCodeValidator.INSTANCE);
        add("Spain",
            "ES",
            "ESP",
            724,
            NullPostalCodeValidator.INSTANCE);
        add("Sri Lanka",
            "LK",
            "LKA",
            144,
            NullPostalCodeValidator.INSTANCE);
        add("Sudan",
            "SD",
            "SDN",
            736,
            NullPostalCodeValidator.INSTANCE);
        add("Suriname",
            "SR",
            "SUR",
            740,
            NullPostalCodeValidator.INSTANCE);
        add("Svalbard and Jan Mayen Islands",
            "SJ",
            "SJM",
            744,
            NullPostalCodeValidator.INSTANCE);
        add("Swaziland",
            "SZ",
            "SWZ",
            748,
            NullPostalCodeValidator.INSTANCE);
        add("Sweden",
            "SE",
            "SWE",
            752,
            NullPostalCodeValidator.INSTANCE);
        add("Switzerland",
            "CH",
            "CHE",
            756,
            NullPostalCodeValidator.INSTANCE);
        add("Syrian Arab Republic (Syria)",
            "SY",
            "SYR",
            760,
            NullPostalCodeValidator.INSTANCE);
        add("Taiwan, Republic of China",
            "TW",
            "TWN",
            158,
            NullPostalCodeValidator.INSTANCE);
        add("Tajikistan",
            "TJ",
            "TJK",
            762,
            NullPostalCodeValidator.INSTANCE);
        add("Tanzania, United Republic of",
            "TZ",
            "TZA",
            834,
            NullPostalCodeValidator.INSTANCE);
        add("Thailand",
            "TH",
            "THA",
            764,
            NullPostalCodeValidator.INSTANCE);
        add("Timor-Leste",
            "TL",
            "TLS",
            626,
            NullPostalCodeValidator.INSTANCE);
        add("Togo",
            "TG",
            "TGO",
            768,
            NullPostalCodeValidator.INSTANCE);
        add("Tokelau",
            "TK",
            "TKL",
            772,
            NullPostalCodeValidator.INSTANCE);
        add("Tonga",
            "TO",
            "TON",
            776,
            NullPostalCodeValidator.INSTANCE);
        add("Trinidad and Tobago",
            "TT",
            "TTO",
            780,
            NullPostalCodeValidator.INSTANCE);
        add("Tunisia",
            "TN",
            "TUN",
            788,
            NullPostalCodeValidator.INSTANCE);
        add("Turkey",
            "TR",
            "TUR",
            792,
            NullPostalCodeValidator.INSTANCE);
        add("Turkmenistan",
            "TM",
            "TKM",
            795,
            NullPostalCodeValidator.INSTANCE);
        add("Turks and Caicos Islands",
            "TC",
            "TCA",
            796,
            NullPostalCodeValidator.INSTANCE);
        add("Tuvalu",
            "TV",
            "TUV",
            798,
            NullPostalCodeValidator.INSTANCE);
        add("Uganda",
            "UG",
            "UGA",
            800,
            NullPostalCodeValidator.INSTANCE);
        add("Ukraine",
            "UA",
            "UKR",
            804,
            NullPostalCodeValidator.INSTANCE);
        add("United Arab Emirates",
            "AE",
            "ARE",
            784,
            NullPostalCodeValidator.INSTANCE);
        add("United Kingdom",
            "GB",
            "GBR",
            826,
            NullPostalCodeValidator.INSTANCE);
        add("United States of America",
            "US",
            "USA",
            840,
            new RegexPostalCodeValidator("(\\d){5}?"));
        add("United States Minor Outlying Islands",
            "UM",
            "UMI",
            581,
            NullPostalCodeValidator.INSTANCE);
        add("Uruguay",
            "UY",
            "URY",
            858,
            NullPostalCodeValidator.INSTANCE);
        add("Uzbekistan",
            "UZ",
            "UZB",
            860,
            NullPostalCodeValidator.INSTANCE);
        add("Vanuatu",
            "VU",
            "VUT",
            548,
            NullPostalCodeValidator.INSTANCE);
        add("Venezuela (Bolivarian Republic of)",
            "VE",
            "VEN",
            862,
            NullPostalCodeValidator.INSTANCE);
        add("Viet Nam",
            "VN",
            "VNM",
            704,
            NullPostalCodeValidator.INSTANCE);
        add("Virgin Islands, US",
            "VI",
            "VIR",
            850,
            NullPostalCodeValidator.INSTANCE);
        add("Wallis and Futuna Islands",
            "WF",
            "WLF",
            876,
            NullPostalCodeValidator.INSTANCE);
        add("Western Sahara",
            "EH",
            "ESH",
            732,
            NullPostalCodeValidator.INSTANCE);
        add("Yemen",
            "YE",
            "YEM",
            887,
            NullPostalCodeValidator.INSTANCE);
        add("Zambia",
            "ZM",
            "ZMB",
            894,
            NullPostalCodeValidator.INSTANCE);
        add("Zimbabwe",
            "ZW",
            "ZWE",
            716,
            NullPostalCodeValidator.INSTANCE);
    }

    private Country(final String nm,
                    final String i2,
                    final String i3,
                    final int iN,
                    final PostalCodeValidator v)
    {
        postalCodeValidator = v;
        name = nm;
        iso2 = i2;
        iso3 = i3;
        isoNumeric = iN;
    }

    @Override
    public int hashCode()
    {
        return (name.hashCode());
    }

    public boolean equals(final Object o)
    {
        final boolean retVal;

        if(o instanceof Country)
        {
            retVal = false;
        }
        else
        {
            final Country other;

            other = (Country) o;
            retVal = name.equals(other.name);
        }

        return (retVal);
    }

    public String getName()
    {
        return (name);
    }

    public String getISO2()
    {
        return (iso2);
    }

    public String getISO3()
    {
        return (iso3);
    }

    public int getISONumeric()
    {
        return (isoNumeric);
    }

    public PostalCodeValidator getPostalCodeValidator()
    {
        return (postalCodeValidator);
    }

    private static void add(final String name,
                            final String iso2,
                            final String iso3,
                            final int isoN,
                            final PostalCodeValidator validator)
    {
        final Country country;

        country = new Country(name,
                              iso2,
                              iso3,
                              isoN,
                              validator);

        if(countriesByName.containsKey(name.toLowerCase()))
        {
            throw new IllegalStateException("countriesByName already contains a value for: " + name.toLowerCase());
        }

        if(countriesByISO2.containsKey(iso2.toLowerCase()))
        {
            throw new IllegalStateException("countriesByISO2 already contains a value for: " + iso2.toLowerCase());
        }

        if(countriesByISO3.containsKey(iso3.toLowerCase()))
        {
            throw new IllegalStateException("countriesByISO3 already contains a value for: " + iso3.toLowerCase());
        }

        if(countriesByISONumber.containsKey(isoN))
        {
            throw new IllegalStateException("countriesByISONumber already contains a value for: " + isoN);
        }

        countriesByName.put(name.toLowerCase(),
                            country);
        countriesByISO2.put(iso2.toLowerCase(),
                            country);
        countriesByISO3.put(iso3.toLowerCase(),
                            country);
        countriesByISONumber.put(isoN,
                                 country);
    }

    public static Country getCountryByName(final String name)
    {
        final Country country;

        country = countriesByName.get(name.toLowerCase());

        return (country);
    }

    public static Country getCountryByISO2(final String iso2)
    {
        final Country country;

        country = countriesByISO2.get(iso2.toLowerCase());

        return (country);
    }

    public static Country getCountryByISO3(final String iso3)
    {
        final Country country;

        country = countriesByISO3.get(iso3.toLowerCase());

        return (country);
    }

    public static Country getCountryByISONumber(final int number)
    {
        final Country country;

        country = countriesByISONumber.get(number);

        return (country);
    }
}
