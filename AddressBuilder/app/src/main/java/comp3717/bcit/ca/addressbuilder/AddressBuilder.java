package comp3717.bcit.ca.addressbuilder;

import java.io.File;

/**
 * Created by darcy on 2016-10-11.
 */

public class AddressBuilder
{
    /**
     * Build a mailing address.
     *
     * @param firstName the first name of the recipient.
     * @param lastName the last name of the recipient.
     * @param buildingNumber the building number of the recipient.
     * @param streetName the street name of the recipient.
     * @param cityName the city name of the recipient.
     * @param provinceName the province name of the recipient.
     * @param country the country of the recipient.
     * @param postalCode the postal code of the recipient.
     *
     * @return the formatted mailing address.
     */
    public String buildAddress(final String  firstName,
                               final String  lastName,
                               final int     buildingNumber,
                               final String  streetName,
                               final String  cityName,
                               final String  provinceName,
                               final Country country,
                               final String  postalCode)
        throws InvalidAddressException
    {
        final StringBuilder builder;

        Checks.checkNotNullOrEmpty("firstName",    firstName);
        Checks.checkNotNullOrEmpty("lastName",     lastName);
        Checks.checkPositive("buildingNumber",     buildingNumber);
        Checks.checkNotNullOrEmpty("streetName",   streetName);
        Checks.checkNotNullOrEmpty("cityName",     cityName);
        Checks.checkNotNullOrEmpty("provinceName", provinceName);
        Checks.checkNotNull("country",             country);
        Checks.checkPostalCode("postalCode", postalCode, country);

        builder = new StringBuilder();
        builder.append(lastName);
        builder.append(", ");
        builder.append(firstName);
        builder.append(File.separator);
        builder.append(buildingNumber);
        builder.append(", ");
        builder.append(streetName);
        builder.append(File.separator);
        builder.append(cityName);
        builder.append(File.separator);
        builder.append(provinceName);
        builder.append(File.separator);
        builder.append(country.getName());
        builder.append(File.separator);
        builder.append(postalCode);

        return (builder.toString());
    }
}
