package comp3717.bcit.ca.addressbuilder;

/**
 * Created by darcy on 2016-10-13.
 */

public class InvalidAddressException
    extends Exception
{
    public InvalidAddressException(final String msg)
    {
        super(msg);
    }
}
