package comp3717.bcit.ca.addressbuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by darcy on 2016-10-14.
 */

public class RegexPostalCodeValidator
    implements PostalCodeValidator
{
    private final Pattern pattern;

    public RegexPostalCodeValidator(final String regex)
    {
        pattern = Pattern.compile(regex);
    }

    public boolean validate(final String postalCode)
    {
        final Matcher matcher;
        final boolean match;

        matcher = pattern.matcher(postalCode);
        match   = matcher.matches();

        return (match);
    }
}
