package comp3717.bcit.ca.addressbuilder;

/**
 * Created by darcy on 2016-10-13.
 */

public final class Checks
{
    private Checks()
    {
    }

    public static final void checkNotNull(final String name,
                                          final Object value)
        throws InvalidAddressException
    {
        if(value == null)
        {
            throw new InvalidAddressException(name + " cannot be null");
        }
    }

    public static final void checkNotNullOrEmpty(final String name,
                                                 final String value)
        throws InvalidAddressException
    {
        checkNotNull(name, value);

        if(value.isEmpty())
        {
            throw new InvalidAddressException(name + " cannot be empty");
        }
    }

    public static final void checkPositive(final String name,
                                           final int    value)
        throws InvalidAddressException
    {
        if(value < 1)
        {
            throw new InvalidAddressException(name + " must be > 0, was: " + value);
        }
    }

    public static final void checkPostalCode(final String  name,
                                             final String  value,
                                             final Country country)
        throws InvalidAddressException
    {
        final PostalCodeValidator validator;

        checkNotNullOrEmpty(name, value);

        validator = country.getPostalCodeValidator();

        if(!(validator.validate(value)))
        {
            throw new InvalidAddressException(name + " is not valid for: " + country.getName());
        }
    }
}
