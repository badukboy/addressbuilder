package comp3717.bcit.ca.addressbuilder;

/**
 * Created by darcy on 2016-10-14.
 */

public interface PostalCodeValidator
{
    boolean validate(String postalCode);
}
