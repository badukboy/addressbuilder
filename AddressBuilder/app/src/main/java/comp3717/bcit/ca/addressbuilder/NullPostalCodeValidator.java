package comp3717.bcit.ca.addressbuilder;

/**
 * Created by darcy on 2016-10-14.
 */

public final class NullPostalCodeValidator
    implements PostalCodeValidator
{
    public static NullPostalCodeValidator INSTANCE;

    static
    {
        INSTANCE = new NullPostalCodeValidator();
    }

    private NullPostalCodeValidator()
    {
    }

    public boolean validate(final String postalCode)
    {
        return (true);
    }
}
